package com.bieniek.restspring.rpc;

import com.bieniek.restspring.rpc.Strategy.DeveloperWageCalculationStrategy;
import com.bieniek.restspring.rpc.Strategy.WageCalculationStrategy;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

public class QueueConsumerTest {
    @Test
    public void getStrategyDeveloper() throws InstantiationException, IllegalAccessException {
        QueueConsumer queueConsumer = new QueueConsumer();

        WageCalculationStrategy strategy = queueConsumer.getStrategy("developer");

        assertThat(strategy, instanceOf(DeveloperWageCalculationStrategy.class));
    }

    @Test(expected = NullPointerException.class)
    public void getUnknownStrategyDeveloper() throws InstantiationException, IllegalAccessException {
         QueueConsumer queueConsumer = new QueueConsumer();

        queueConsumer.getStrategy("unknown");
    }
}