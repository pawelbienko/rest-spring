package com.bieniek.restspring.rest.controller;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmployeeControllerTest {

    @Autowired
    private TestRestTemplate template;

    @Test
    public void nonexistentUserCannotGetToken() throws Exception {
        String username = "admin";
        String password = "12345";

        String body = "{\"username\":\"" + username + "\", \"password\":\"" + password + "\"}";

        // Given
        HttpUriRequest request = new HttpGet( "http://localhost/employees");

        // When
        CloseableHttpResponse httpResponse = HttpClientBuilder.create().build().execute( request );

        // Then
        assertSame(httpResponse.getStatusLine().getStatusCode(), (HttpStatus.BAD_REQUEST));
    }
}