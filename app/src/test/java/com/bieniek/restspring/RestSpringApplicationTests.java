package com.bieniek.restspring;

import com.bieniek.restspring.rest.employee.EmployeeRepository;
import com.bieniek.restspring.security.repository.AppUserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestSpringApplicationTests {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	AppUserRepository appUserRepository;

	@Test
	public void contextLoads() {
		assertEquals(2, employeeRepository.findAll().size());
		assertEquals(2, appUserRepository.findAll().size());
	}

}

