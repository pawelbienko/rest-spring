package com.bieniek.restspring.security;

import java.util.List;

import com.bieniek.restspring.security.model.AppUser;

import com.bieniek.restspring.security.repository.AppUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired AppUserRepository appUserRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    log.info("Started loading user:", username);

    AppUser appUser = appUserRepository.findByUsername(username);

    log.info("Found user:", appUser.getId());

    if (!appUser.equals(null)) {
      List<GrantedAuthority> grantedAuthorities =
          AuthorityUtils.commaSeparatedStringToAuthorityList(appUser.getRole());

      log.info("Created user:");
      return new User(appUser.getUsername(), appUser.getPassword(), grantedAuthorities);
    }

    throw new UsernameNotFoundException("Username: " + username + " not found");
  }
}
