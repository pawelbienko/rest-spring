package com.bieniek.restspring;

import com.bieniek.restspring.rest.employee.EmployeeRepository;
import com.bieniek.restspring.rest.employee.Employee;
import com.bieniek.restspring.rest.employee.Role;
import com.bieniek.restspring.rest.employee.Status;
import com.bieniek.restspring.security.model.AppUser;
import com.bieniek.restspring.security.repository.AppUserRepository;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@Slf4j
public class LoadDatabase {

  @Autowired EmployeeRepository employeeRepository;

  @Autowired private BCryptPasswordEncoder encoder;

  @Autowired AppUserRepository appUserRepository;

  @Bean
  public boolean init() {
    log.info("Started loading database");

    employeeRepository.save(new Employee("Bilbo", "Baggins", "active", "developer"));
    employeeRepository.save(new Employee("Frodo", "Baggins", "active", "tester"));

    appUserRepository.save(new AppUser("user", encoder.encode("12345"), "ROLE_USER"));
    appUserRepository.save(new AppUser("admin", encoder.encode("12345"), "ROLE_ADMIN"));

    log.info("Completed loading database");

    return true;
  }
}
