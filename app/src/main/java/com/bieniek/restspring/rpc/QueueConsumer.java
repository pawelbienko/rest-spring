package com.bieniek.restspring.rpc;

import com.bieniek.restspring.rest.employee.Employee;
import com.bieniek.restspring.rest.employee.EmployeeRepository;
import com.bieniek.restspring.rpc.Strategy.DeveloperWageCalculationStrategy;
import com.bieniek.restspring.rpc.Strategy.ManagerWageCalculationStrategy;
import com.bieniek.restspring.rpc.Strategy.TesterWageCalculationStrategy;
import com.bieniek.restspring.rpc.Strategy.WageCalculationStrategy;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
class QueueConsumer {

  @Autowired
  EmployeeRepository employeeRepository;

  Map<String, Class> strategies =  new HashMap<String, Class>(){
    {
      put("developer", DeveloperWageCalculationStrategy.class);
      put("tester", TesterWageCalculationStrategy.class);
      put("manager", ManagerWageCalculationStrategy.class);
    }
  };

  WageCalculationStrategy getStrategy(String strategyName) throws IllegalAccessException, InstantiationException{
    return (WageCalculationStrategy) strategies.get(strategyName).newInstance();
  }

  @RabbitListener(queues = "${queue.name}")
  @Transactional
  private void reader(Long employeeId) throws InstantiationException, IllegalAccessException {
    Employee employee = employeeRepository.findById(employeeId).orElse(null);

    Integer wage = getStrategy(employee.getRole()).calculate(employee);
    employee.setWage(wage);

    employeeRepository.save(employee);
  }
}
