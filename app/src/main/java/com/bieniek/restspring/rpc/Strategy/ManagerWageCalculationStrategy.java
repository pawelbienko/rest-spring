package com.bieniek.restspring.rpc.Strategy;

import com.bieniek.restspring.rest.employee.Employee;

public class ManagerWageCalculationStrategy  implements WageCalculationStrategy {
    @Override
    public Integer calculate(Employee employee) {
        return 1200;
    }
}
