package com.bieniek.restspring.rpc;

import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@AutoJsonRpcServiceImpl
public class WageCalculatorService implements CalculatorServiceApi {
    @Value("${queue.name}")
    private String queueName;

    @Autowired
    RabbitTemplate queueSender;

    @Override
    public HttpStatus calculate(Long employeeId){
        queueSender.convertAndSend(queueName, employeeId);
        return HttpStatus.ACCEPTED;
    }
}
