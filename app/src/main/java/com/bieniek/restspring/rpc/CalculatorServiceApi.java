package com.bieniek.restspring.rpc;

import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;
import org.springframework.http.HttpStatus;

@JsonRpcService("/calculator")
public interface CalculatorServiceApi {
    HttpStatus calculate(@JsonRpcParam(value = "employeeId") Long employeeId);
}

