package com.bieniek.restspring.rpc.Strategy;

import com.bieniek.restspring.rest.employee.Employee;

public interface WageCalculationStrategy {
    public Integer calculate(Employee employee);
}
