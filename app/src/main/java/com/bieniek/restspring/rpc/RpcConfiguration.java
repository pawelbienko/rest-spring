package com.bieniek.restspring.rpc;

import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImplExporter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RpcConfiguration {

  @Bean
  public static AutoJsonRpcServiceImplExporter autoJsonRpcServiceImplExporter() {
    AutoJsonRpcServiceImplExporter exp = new AutoJsonRpcServiceImplExporter();
    // in here you can provide custom HTTP status code providers etc. eg:
    // exp.setHttpStatusCodeProvider();
    // exp.setErrorResolver();
    return exp;
  }
}
