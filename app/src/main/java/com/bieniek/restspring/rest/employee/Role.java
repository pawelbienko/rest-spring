package com.bieniek.restspring.rest.employee;

public enum Role {
    TESTER,
    DEVELOPER,
    MANAGER,
}
