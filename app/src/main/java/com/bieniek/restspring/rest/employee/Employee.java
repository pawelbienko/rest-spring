package com.bieniek.restspring.rest.employee;

import com.bieniek.restspring.rest.employee.Annotation.EnumValidator;
import com.bieniek.restspring.rest.employee.Validator.EnumValidatorImpl;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.Constraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
public class Employee {

  private @Id @GeneratedValue Long id;

  @NotNull
  @Size(min = 2, message = "FirstName should have atleast 2 characters")
  private String firstName;

  @NotNull
  @Size(min = 2, message = "LastName should have atleast 2 characters")
  private String lastName;

  @EnumValidator(providedEnumClass=Status.class)
  private String status;

  @EnumValidator(providedEnumClass=Role.class)
  private String role;

  private Integer wage;

  public Employee(String firstName, String lastName, String status, String role) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.status = status;
    this.role = role;
  }

  public Employee() {}

  public Long getId() {
    return id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getName() {
    return this.firstName + " " + this.lastName;
  }

  public void setName(String name) {
    String[] parts = name.split(" ");
    this.firstName = parts[0];
    this.lastName = parts[1];
  }
}
