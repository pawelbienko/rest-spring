package com.bieniek.restspring.rest.employee;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
public class EmployeeHandler {

  private final EmployeeRepository repository;

  private final EmployeeResourceAssembler assembler;

  EmployeeHandler(EmployeeRepository repository, EmployeeResourceAssembler assembler) {
    this.repository = repository;
    this.assembler = assembler;
  }

  Resources<Resource<Employee>> getAll() {
    List<Resource<Employee>> employees =
        repository.findAll().stream().map(assembler::toResource).collect(Collectors.toList());

    return new Resources<>(
        employees, linkTo(methodOn(EmployeeController.class).all()).withSelfRel());
  }

  ResponseEntity<?> addNew(Employee newEmployee) throws URISyntaxException {
    Resource<Employee> resource = assembler.toResource(repository.save(newEmployee));

    return ResponseEntity.created(new URI(resource.getId().expand().getHref())).body(resource);
  }

  Resource<Employee> getOne(Long id) {
    Employee employee =
        repository.findById(id).orElseThrow(() -> new EmployeeNotFoundException(id));

    return assembler.toResource(employee);
  }

  ResponseEntity<?> replace(Employee newEmployee, Long id) throws URISyntaxException {
    Employee updatedEmployee =
        repository
            .findById(id)
            .map(
                employee -> {
                  employee.setName(newEmployee.getName());
                  employee.setRole(newEmployee.getRole());
                  return repository.save(employee);
                })
            .orElseGet(
                () -> {
                  newEmployee.setId(id);
                  return repository.save(newEmployee);
                });

    Resource<Employee> resource = assembler.toResource(updatedEmployee);

    return ResponseEntity.created(new URI(resource.getId().expand().getHref())).body(resource);
  }

  ResponseEntity<?> delete(Long id) {
    Employee employee =
        repository.findById(id).orElseThrow(() -> new EmployeeNotFoundException(id));

    if (employee.getStatus() == Status.ACTIVE.toString()) {
      employee.setStatus(Status.INACTIVE.toString());
      return ResponseEntity.ok(assembler.toResource(repository.save(employee)));
    }

    return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED)
        .body(
            new VndErrors.VndError(
                "Method not allowed",
                "You can't remove employee that is in the " + employee.getStatus() + " status"));
  }
}
