package com.bieniek.restspring.rest.employee;

import java.net.URISyntaxException;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class EmployeeController {

  private final EmployeeHandler employeeHandler;

  EmployeeController(EmployeeHandler employeeHandler) {
    this.employeeHandler = employeeHandler;
  }

  @GetMapping("/employees")
  public Resources<Resource<Employee>> all() {
    return employeeHandler.getAll();
  }

  @PostMapping("/employees")
  ResponseEntity<?> addEmployee(@Valid @RequestBody Employee employee) throws URISyntaxException {
    return employeeHandler.addNew(employee);
  }

  @GetMapping("/employees/{id}")
  public Resource<Employee> getOne(@PathVariable Long id) {
    return employeeHandler.getOne(id);
  }

  @PutMapping("/employees/{id}")
  ResponseEntity<?> replaceEmployee(@Valid @RequestBody Employee employee, @PathVariable Long id)
      throws URISyntaxException {
    return employeeHandler.replace(employee, id);
  }

  @DeleteMapping("/employees/{id}")
  ResponseEntity<?> deleteEmployee(@PathVariable Long id) {
    return employeeHandler.delete(id);
  }
}
