package com.bieniek.restspring.rest.employee;

public enum Status {
  ACTIVE,
  INACTIVE
}
